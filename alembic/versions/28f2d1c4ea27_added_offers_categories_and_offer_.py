"""Added offers,categories and offer_matches table table

Revision ID: 28f2d1c4ea27
Revises: 
Create Date: 2022-02-17 10:12:24.793485

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '28f2d1c4ea27'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('categories',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=False),
    sa.Column('parent_category_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['parent_category_id'], ['categories.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('offers',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('uuid', sa.String(), nullable=False),
    sa.Column('category_id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(), nullable=False),
    sa.Column('description', sa.String(), nullable=False),
    sa.Column('parameters', sa.JSON(), nullable=False),
    sa.ForeignKeyConstraint(['category_id'], ['categories.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('uuid')
    )
    op.create_table('offer_matches',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('source_offer_id', sa.Integer(), nullable=False),
    sa.Column('target_offer_id', sa.Integer(), nullable=False),
    sa.Column('common_params_count', sa.Integer(), nullable=False),
    sa.Column('diff_params_count', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['source_offer_id'], ['offers.id'], ),
    sa.ForeignKeyConstraint(['target_offer_id'], ['offers.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('offer_matches')
    op.drop_table('offers')
    op.drop_table('categories')
    # ### end Alembic commands ###
