import pytest
import uuid

from heureka_test.api import create_app
from heureka_test.db import models, repositories
from tests.unit.db import Session, fake_session  # noqa


@pytest.fixture
def api_app(fake_session):
    app = create_app(fake_session)
    app.config.update({
        "TESTING": True,
    })

    return app


@pytest.fixture
def client(api_app):
    return api_app.test_client()


@pytest.fixture
def fake_category_kwargs(faker):
    return {
        'id': faker.random_int(),
        'name': faker.name(),
        'parent_category': None,
    }


@pytest.fixture
def make_fake_offer_kwargs(faker, fake_category_kwargs):
    def _generate():
        return {
            'id': faker.random_int(),
            'uuid': str(uuid.uuid4()),
            'category': models.Category(**fake_category_kwargs),
            'name': faker.name(),
            'description': faker.paragraph(),
            'parameters': {},
        }
    return _generate


@pytest.fixture
def fake_offer_kwargs(faker, make_fake_offer_kwargs):
    return make_fake_offer_kwargs()


@pytest.fixture
def fake_offer_match_kwargs(faker, make_fake_offer_kwargs):
    return {
        'id': faker.random_int(),
        'source_offer': models.Offer(**make_fake_offer_kwargs()),
        'target_offer': models.Offer(**make_fake_offer_kwargs()),
        'common_params_count': faker.random_int(),
        'diff_params_count': faker.random_int(),
    }


def test_offers_list_endpoint_empty(client, mocker, fake_offer_repository_cls):
    mocker.patch.object(repositories, 'OfferRepository', fake_offer_repository_cls)
    response = client.get('/offers')

    assert response.json == {'offers': []}


def test_offers_list_endpoint(client, mocker, faker, fake_offer_repository_cls, fake_session, fake_offer_kwargs):
    mocker.patch.object(repositories, 'OfferRepository', fake_offer_repository_cls)

    fake_offer_repository_cls(fake_session).add(models.Offer(**fake_offer_kwargs))

    response = client.get('/offers')

    expected_result = fake_offer_kwargs.copy()
    expected_result['category'] = {
        'id': expected_result['category'].id,
        'name': expected_result['category'].name,
        'parent_category': None
    }

    assert response.json.items() == {'offers': [expected_result]}.items()


def test_offer_get_endpoint(client, mocker, faker, fake_offer_repository_cls, fake_session, fake_offer_kwargs):
    mocker.patch.object(repositories, 'OfferRepository', fake_offer_repository_cls)

    fake_offer_repository_cls(fake_session).add(models.Offer(**fake_offer_kwargs))

    response = client.get(f'/offers/{fake_offer_kwargs["id"]}')

    expected_result = fake_offer_kwargs.copy()
    expected_result['category'] = {
        'id': expected_result['category'].id,
        'name': expected_result['category'].name,
        'parent_category': None
    }

    assert response.json.items() == {'offer': expected_result}.items()


def test_offer_get_endpoint_404(client, mocker, faker, fake_offer_repository_cls, fake_session, fake_offer_kwargs):
    mocker.patch.object(repositories, 'OfferRepository', fake_offer_repository_cls)

    fake_offer_repository_cls(fake_session).add(models.Offer(**fake_offer_kwargs))

    response = client.get(f'/offers/{faker.random_int()}')

    assert response.status_code == 404


def test_categories_list_endpoint_empty(client, mocker, fake_category_repository_cls):
    mocker.patch.object(repositories, 'CategoryRepository', fake_category_repository_cls)
    response = client.get('/categories')

    assert response.json == {'categories': []}


def test_categories_list_endpoint(client, mocker, faker, fake_category_repository_cls, fake_session, fake_category_kwargs):
    mocker.patch.object(repositories, 'CategoryRepository', fake_category_repository_cls)

    fake_category_repository_cls(fake_session).add(models.Category(**fake_category_kwargs))

    response = client.get('/categories')

    assert response.json.items() == {'categories': [fake_category_kwargs]}.items()


def test_category_get_endpoint(client, mocker, faker, fake_category_repository_cls, fake_session, fake_category_kwargs):
    mocker.patch.object(repositories, 'CategoryRepository', fake_category_repository_cls)

    fake_category_repository_cls(fake_session).add(models.Category(**fake_category_kwargs))

    response = client.get(f'/categories/{fake_category_kwargs["id"]}')

    assert response.json.items() == {'category': fake_category_kwargs}.items()


def test_category_get_endpoint_404(client, mocker, faker, fake_category_repository_cls, fake_session, fake_category_kwargs):
    mocker.patch.object(repositories, 'CategoryRepository', fake_category_repository_cls)

    fake_category_repository_cls(fake_session).add(models.Category(**fake_category_kwargs))

    response = client.get(f'/categories/{faker.random_int()}')

    assert response.status_code == 404


def test_offer_matches_list_endpoint_empty(client, mocker, fake_offer_match_repository_cls):
    mocker.patch.object(repositories, 'OfferMatchRepository', fake_offer_match_repository_cls)
    response = client.get('/offer-matches')

    assert response.json == {'offer_matches': []}


def test_offer_matches_list_endpoint(client, mocker, faker, fake_offer_match_repository_cls, fake_session, fake_offer_match_kwargs):
    mocker.patch.object(repositories, 'OfferMatchRepository', fake_offer_match_repository_cls)

    fake_offer_match_repository_cls(fake_session).add(models.OfferMatch(**fake_offer_match_kwargs))

    response = client.get('/offer-matches')
    expected_result = fake_offer_match_kwargs.copy()
    expected_result['source_offer'] = expected_result['source_offer'].to_dict()
    expected_result['target_offer'] = expected_result['target_offer'].to_dict()

    assert response.json.items() == {'offer_matches': [expected_result]}.items()


def test_offer_match_get_endpoint(client, mocker, faker, fake_offer_match_repository_cls, fake_session, fake_offer_match_kwargs):
    mocker.patch.object(repositories, 'OfferMatchRepository', fake_offer_match_repository_cls)

    fake_offer_match_repository_cls(fake_session).add(models.OfferMatch(**fake_offer_match_kwargs))

    response = client.get(f'/offer-matches/{fake_offer_match_kwargs["id"]}')
    expected_result = fake_offer_match_kwargs.copy()
    expected_result['source_offer'] = expected_result['source_offer'].to_dict()
    expected_result['target_offer'] = expected_result['target_offer'].to_dict()

    assert response.json.items() == {'offer_match': expected_result}.items()


def test_offer_match_get_endpoint_404(client, mocker, faker, fake_offer_match_repository_cls, fake_session, fake_offer_match_kwargs):
    mocker.patch.object(repositories, 'OfferMatchRepository', fake_offer_match_repository_cls)

    fake_offer_match_repository_cls(fake_session).add(models.OfferMatch(**fake_offer_match_kwargs))

    response = client.get(f'/offer-matches/{faker.random_int()}')

    assert response.status_code == 404
