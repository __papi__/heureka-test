import pytest


class Session:

    def begin(self):
        pass

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


@pytest.fixture
def fake_session(mocker):
    mocker.patch.object(Session, 'begin', autospec=True)
    mocker.patch.object(Session, '__enter__', autospec=True)
    mocker.patch.object(Session, '__exit__', autospec=True)

    return Session()
