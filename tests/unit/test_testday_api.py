import pytest
import uuid

from heureka_test.testday_api import TestDayApi


@pytest.fixture
def testday_api(faker):
    return TestDayApi(endpoint=faker.pystr(), auth_token=faker.pystr())


def test_testdai_api_init(faker):
    endpoint = faker.pystr()
    auth_token = faker.pystr()
    api = TestDayApi(endpoint=endpoint, auth_token=auth_token)

    assert api.endpoint == endpoint
    assert api.auth_token == auth_token


def test_get_offer_matches(mocker, testday_api):
    matching_offers = [
        'feaa400a-d304-4f55-b045-51b1daec8e0c',
        '29e0b669-a670-476b-808a-e21a449d1c0f',
    ]

    def fake_requests_get(endpoint, headers=None):
        class FakeResponse:
            status_code = 200

            def json(self):
                return {
                    'matching_offers': matching_offers
                }
        return FakeResponse()

    mocker.patch('requests.get', fake_requests_get)
    assert testday_api.get_offer_matches(str(uuid.uuid4())) == matching_offers


def test_get_offer_matches_bad_status_code(mocker, testday_api):
    def fake_requests_get(endpoint, headers=None):
        class FakeResponse:
            status_code = 404

        return FakeResponse()

    mocker.patch('requests.get', fake_requests_get)
    assert testday_api.get_offer_matches(str(uuid.uuid4())) == []
