import typing
import pytest

from sqlalchemy.exc import NoResultFound

from heureka_test.db import repositories, models


@pytest.fixture
def fake_offer_repository_cls():
    class FakeOfferRepository(repositories.AbstractRepository):
        data = set()

        def add(self, offer: models.Offer):
            self.data.add(offer)

        def add_or_update(self, offer: models.Offer):
            # Find existing offer by UUID
            existing_offer = None
            for entry in self.data:
                if entry.uuid == offer.uuid:
                    existing_offer = entry
                    break

            if existing_offer:
                existing_offer.category_id = offer.category_id
                existing_offer.name = offer.name
                existing_offer.description = offer.description
                existing_offer.parameters = offer.parameters
                return existing_offer
            else:
                self.add(offer)
                return offer

        def get(self, id: int) -> models.Offer:
            for entry in self.data:
                if entry.id == id:
                    return entry

        def get_by_uuid(self, uuid: str) -> models.Offer:
            for entry in self.data:
                if entry.uuid == uuid:
                    return entry

        def list(self) -> typing.List[models.Offer]:
            return list(self.data)

        def delete(self, offer: models.Offer):
            pass

    return FakeOfferRepository


@pytest.fixture
def fake_category_repository_cls():
    class FakeCategoryRepository(repositories.AbstractRepository):
        data = set()

        def add(self, category: models.Category):
            self.data.add(category)

        def add_or_update(self, category: models.Category):
            try:
                category = self.get_by_name(category.name)
                category.parent_category = category.parent_category
            except NoResultFound:
                self.add(category)

            return category

        def get(self, id: int):
            for entry in self.data:
                if entry.id == id:
                    return entry

        def get_by_name(self, name: str) -> typing.Optional[models.Category]:
            for entry in self.data:
                if entry.name == name:
                    return entry

            raise NoResultFound('No row was found')

        def get_by_name_or_create(self, name: str) -> models.Category:
            try:
                category = self.get_by_name(name)
            except NoResultFound:
                category = models.Category(name=name)
                self.add(category)

            return category

        def list(self) -> typing.List[models.Category]:
            return list(self.data)

        def delete(self, category: models.Category):
            pass

    return FakeCategoryRepository


@pytest.fixture
def fake_offer_match_repository_cls():
    class FakeOfferMatchRepository(repositories.AbstractRepository):
        data = set()

        def add(self, offer_match: models.OfferMatch):
            self.data.add(offer_match)

        def add_or_update(self, offer_match: models.OfferMatch):
            existing_offer_match = None

            for entry in self.data:
                if (entry.source_offer == offer_match.source_offer and entry.target_offer == offer_match.target_offer) \
                    or (entry.source_offer == offer_match.target_offer
                        and entry.target_offer == offer_match.source_offer):

                    existing_offer_match = entry

            if existing_offer_match:
                existing_offer_match.common_params_count = offer_match.common_params_count
                existing_offer_match.diff_params_count = offer_match.diff_params_count
                return existing_offer_match
            else:
                self.data.add(offer_match)
                return offer_match

        def get(self, id: int) -> models.OfferMatch:
            for entry in self.data:
                if entry.id == id:
                    return entry

        def list(self) -> typing.List[models.OfferMatch]:
            return list(self.data)

        def delete(self, offer_match: models.OfferMatch):
            pass

    return FakeOfferMatchRepository
