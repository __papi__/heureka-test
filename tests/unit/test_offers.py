import pytest

from heureka_test import offers


@pytest.fixture
def params_dict_diff_values():
    return \
        {
            'a': 43,
            'b': None,
            'c': 'foo',
        }, {
            'a': 42,
            'b': False,
            'c': 'foo',
        }


@pytest.fixture
def params_dict_extra_keys():
    return \
        {
            'a': 43,
            'b': None,
        }, {
            'a': 43,
            'b': None,
            'c': 'foo',
            'd': 'bar',
        }


@pytest.fixture
def params_dict_less_keys():
    return \
        {
            'a': 43,
            'b': None,
            'c': 'foo',
            'd': 'bar',
        }, {
            'a': 43,
            'b': None,
        }


@pytest.fixture
def params_dict_extra_keys_diff_values():
    return \
        {
            'a': 43,
            'b': True,
        }, {
            'a': 43,
            'b': None,
            'c': 'foo',
            'd': 'bar',
        }


@pytest.fixture
def params_dict_less_keys_diff_values():
    return \
        {
            'a': 43,
            'b': True,
            'c': 'foo',
            'd': 'bar',
        }, {
            'a': 43,
            'b': None,
        }


def test_find_common_parameters_diff_values(params_dict_diff_values):
    assert offers.find_common_parameters_len(*params_dict_diff_values) == 1


def test_find_common_parameters_extra_keys(params_dict_extra_keys):
    assert offers.find_common_parameters_len(*params_dict_extra_keys) == 2


def test_find_common_parameters_less_keys(params_dict_less_keys):
    assert offers.find_common_parameters_len(*params_dict_less_keys) == 2


def test_find_common_parameters_extra_keys_diff_values(params_dict_extra_keys_diff_values):
    assert offers.find_common_parameters_len(*params_dict_extra_keys_diff_values) == 1


def test_find_common_parameters_less_keys_diff_values(params_dict_less_keys_diff_values):
    assert offers.find_common_parameters_len(*params_dict_less_keys_diff_values) == 1


def test_find_common_parameters_transitivity(params_dict_less_keys_diff_values):
    assert offers.find_common_parameters_len(*params_dict_less_keys_diff_values) == offers.find_common_parameters_len(
        params_dict_less_keys_diff_values[1], params_dict_less_keys_diff_values[0])


def test_find_diff_parameters_diff_values(params_dict_diff_values):
    assert offers.find_diff_parameters_len(*params_dict_diff_values) == 2


def test_find_diff_parameters_extra_keys(params_dict_extra_keys):
    assert offers.find_diff_parameters_len(*params_dict_extra_keys) == 2


def test_find_diff_parameters_less_keys(params_dict_less_keys):
    assert offers.find_diff_parameters_len(*params_dict_less_keys) == 2


def test_find_diff_parameters_extra_keys_diff_values(params_dict_extra_keys_diff_values):
    assert offers.find_diff_parameters_len(*params_dict_extra_keys_diff_values) == 3


def test_find_diff_parameters_less_keys_diff_values(params_dict_less_keys_diff_values):
    assert offers.find_diff_parameters_len(*params_dict_less_keys_diff_values) == 3


def test_find_diff_parameters_transitivity(params_dict_less_keys_diff_values):
    assert offers.find_diff_parameters_len(*params_dict_less_keys_diff_values) == offers.find_diff_parameters_len(
        params_dict_less_keys_diff_values[1], params_dict_less_keys_diff_values[0])
