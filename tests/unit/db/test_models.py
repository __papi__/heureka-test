import uuid

from heureka_test.db import models


def test_category_model_to_dict(faker):
    name = faker.name()
    category = models.Category(name=name)

    assert category.to_dict().items() >= {'name': name, 'parent_category': None}.items()


def test_category_model_with_parent_to_dict(faker):
    name = faker.name()
    parent_name = faker.name()
    category = models.Category(name=name, parent_category=models.Category(name=parent_name))

    dict_category = category.to_dict()

    assert dict_category['name'] == name
    assert dict_category['parent_category'].items() >= {'name': parent_name, 'parent_category': None}.items()


def test_offer_model_to_dict(faker):
    kwargs = {
        'uuid': uuid.uuid4(),
        'category': models.Category(name=faker.name()),
        'name': faker.name(),
        'description': faker.paragraph(),
        'parameters': {},
    }
    offer = models.Offer(**kwargs)

    expected_dict = kwargs.copy()
    expected_dict['category'] = {
        'id': kwargs['category'].id,
        'name': kwargs['category'].name,
        'parent_category': kwargs['category'].parent_category
    }
    assert offer.to_dict().items() >= expected_dict.items()


def test_offer_match_model_to_dict(faker):
    source_offer_kwargs = {
        'uuid': uuid.uuid4(),
        'category': models.Category(name=faker.name()),
        'name': faker.name(),
        'description': faker.paragraph(),
        'parameters': {},
    }
    target_offer_kwargs = {
        'uuid': uuid.uuid4(),
        'category': models.Category(name=faker.name()),
        'name': faker.name(),
        'description': faker.paragraph(),
        'parameters': {},
    }
    source_offer = models.Offer(**source_offer_kwargs)
    target_offer = models.Offer(**target_offer_kwargs)
    offer_match_kwargs = {
        'source_offer': source_offer,
        'target_offer': target_offer,
        'common_params_count': faker.random_int(3, 10),
        'diff_params_count': faker.random_int(3, 10),
    }
    offer_match = models.OfferMatch(**offer_match_kwargs)

    expected_dict = offer_match_kwargs.copy()
    expected_dict['source_offer'] = source_offer.to_dict()
    expected_dict['target_offer'] = target_offer.to_dict()

    assert offer_match.to_dict().items() >= expected_dict.items()
