import sqlalchemy.orm.session
import pytest

import heureka_test.messages
from heureka_test.db import repositories
from tests.unit.db import Session, fake_session  # noqa
from tests.fake_testday_api import FakeTestDayApi


@pytest.fixture
def kafka_offer_message():
    return {
        'metadata': {
            'type': 'offer'
        },
        'payload': {
            'id': 'feaa400a-d304-4f55-b045-51b1daec8e0c',
            'category': 'LEGO',
            'name': 'LEGO Harry Potter - Roxfort (71043)',
            'description': 'Üdvözlünk az egyedülálló LEGO® Harry Potter™ 71043 Roxfort kastélyban! Építsd meg és '
                           'állítsd ki ezt a részletesen kidolgozott mini LEGO® Harry Potter TM Roxfort kastély '
                           'modellt, mely több mint 6000 elemből áll! Fedezd fel a rendkívül részletesen kidolgozott '
                           'kamrákat, tornyokat és tantermeket, valamint számos rejtett funkciót és a Harry Potter '
                           'filmek jeleneteit is! Népesítsd be a kastélyt 27 mikrofigurával, melyek között Harry, '
                           'Hermione és Ron figurája is szerepel, továbbá rengeteg jellegzetes kiegészítő és tárgy '
                           'lenyűgöző választéka is vár rád! A varázslatos építési élményt pedig kiegészítheted '
                           'Hagrid kunyhójával és a Fúriafűzzel.\n\n\n\nÍgy is ismerheti: Harry Potter Roxfort 71043, '
                           'HarryPotterRoxfort71043, Harry Potter Roxfort (71043), HarryPotter-Roxfort71043, '
                           'Harry Potter - Roxfort ( 71043)',
            'parameters': {
                'minimum age': 16,
                'set': 'Harry Potter',
                'number of pieces': 6020
            }
        }
    }


@pytest.fixture
def kafka_offer_message2():
    return {
        'metadata': {
            'type': 'offer'
        },
        'payload': {
            'id': '29e0b669-a670-476b-808a-e21a449d1c0f',
            'category': 'Books',
            'name': 'Harry Potter a Kámen mudrců - J. K. Rowlingová',
            'description': 'Harryho Pottera Vám zajisté nemusím představovat, neboť je to taková knižní legenda a '
                           'téměř už i klasika. Pro mnoho z Vás je to dokonce kniha, na které jste vyrostli.\n\nKdyž '
                           'byl Harrymu jeden rok, tak přišel o své rodiče. Ten - o kom se nemluví - zavraždil '
                           'Harryho rodiče a chtěl zabít i jeho samotného, jenže nějakým zázrakem chlapec přežil a na '
                           'čele mu zůstala jizva v podobě blesku. Chlapec deset let vyrůstal u strýce a tety '
                           'Dursleyových a jejich syna Dudleyho. Bylo by pravdě podobné, že když se jedná o přímé '
                           'příbuzné, že bude o chlapce postaráno dobře, ale Harry doslova trpěl a je div, '
                           'že z něho vyrostl bystrý a hodný chlapec.\n\nNezná svou minulost, nezná své rodiče, '
                           'bylo mu jen řečeno, že zemřeli při nehodě. Neví z jaké rodiny pochází a ani neví, '
                           'co mu bylo dáno do vínku. Ví, že je jiný, že se občas stanou zvláštní věci, ale ani ve '
                           'snu by ho nenapadlo, že v den svých jedenáctých narozenin zjistí pravdu, která mu navždy '
                           'změní život. A i přesto, že se teta se strýcem snaží všemožně zabránit tomu, aby se Harry '
                           'dozvěděl skutečnost o sobě i svých rodičích, se jednoho dne objeví sova se zvláštním '
                           'dopisem z Bradavic - ze školy čar a kouzel, jejímž ředitelem je Albus Brumbál.\n\nK '
                           'příběhu asi netřeba více dodávat, kdo by jej neznal? Věřím, že není nikdo, i třeba těch, '
                           'kteří knihy nečetli a kdo by nevěděl kdo je Harry Potter. Víte, že letos je to dvacet let '
                           'od vydání první knihy od skvělé autorky J.K. Rowling?',
            'parameters': {
                'author': 'J.K. Rowling',
                'genre': 'for children',
                'publisher': 'Albatros',
                'number of pages': '336',
                'year': '2017',
                'language': 'czech'
            }
        }
    }


@pytest.fixture
def kafka_category_message():
    return {
        'metadata': {
            'type': 'category'
        },
        'payload': {
            'name': 'LEGO',
            'parent_category': 'Building kits'
        }
    }


@pytest.fixture
def kafka_category_message_without_parent_category():
    return {
        'metadata': {
            'type': 'category'
        },
        'payload': {
            'name': 'LEGO',
        }
    }


def test_process_offer_message(fake_session, mocker, kafka_offer_message, fake_offer_repository_cls, fake_category_repository_cls):
    mocker.patch.object(repositories, 'OfferRepository', fake_offer_repository_cls)
    mocker.patch.object(repositories, 'CategoryRepository', fake_category_repository_cls)
    mocker.patch.object(sqlalchemy.orm.session.Session, 'refresh', autospec=True)

    heureka_test.messages.process_message(FakeTestDayApi(), fake_session, kafka_offer_message)

    offer = fake_offer_repository_cls(None).get_by_uuid(kafka_offer_message['payload']['id'])

    assert offer.uuid == kafka_offer_message['payload']['id']
    assert offer.category.name == kafka_offer_message['payload']['category']
    assert offer.name == kafka_offer_message['payload']['name']
    assert offer.description == kafka_offer_message['payload']['description']
    assert offer.parameters == kafka_offer_message['payload']['parameters']


def test_process_offer_message_matching_offers(fake_session, mocker, kafka_offer_message, kafka_offer_message2,
                                               fake_offer_repository_cls, fake_category_repository_cls,
                                               fake_offer_match_repository_cls):
    mocker.patch.object(repositories, 'OfferRepository', fake_offer_repository_cls)
    mocker.patch.object(repositories, 'CategoryRepository', fake_category_repository_cls)
    mocker.patch.object(repositories, 'OfferMatchRepository', fake_offer_match_repository_cls)

    mocker.patch.object(sqlalchemy.orm.session.Session, 'refresh', autospec=True)
    heureka_test.messages.process_message(FakeTestDayApi(), fake_session, kafka_offer_message)
    heureka_test.messages.process_message(FakeTestDayApi(), fake_session, kafka_offer_message2)

    offer = fake_offer_repository_cls(None).get_by_uuid(kafka_offer_message['payload']['id'])
    offer2 = fake_offer_repository_cls(None).get_by_uuid(kafka_offer_message2['payload']['id'])
    offer_matches = fake_offer_match_repository_cls(None).list()

    for o, msg in [(offer, kafka_offer_message), (offer2, kafka_offer_message2)]:
        assert o.uuid == msg['payload']['id']
        assert o.category.name == msg['payload']['category']
        assert o.name == msg['payload']['name']
        assert o.description == msg['payload']['description']
        assert o.parameters == msg['payload']['parameters']

    assert len(offer_matches) == 1
    for offer_match in offer_matches:
        assert offer_match.source_offer.uuid == offer2.uuid
        assert offer_match.target_offer.uuid == offer.uuid


def test_process_category_message(fake_session, mocker, kafka_category_message, fake_category_repository_cls):
    mocker.patch.object(repositories, 'CategoryRepository', fake_category_repository_cls)

    heureka_test.messages.process_message(FakeTestDayApi(), fake_session, kafka_category_message)

    category = fake_category_repository_cls(None).get_by_name(kafka_category_message['payload']['name'])

    assert category.name == kafka_category_message['payload']['name']
    assert category.parent_category.name == kafka_category_message['payload']['parent_category']


def test_process_category_message_without_parent_category(fake_session, mocker, kafka_category_message_without_parent_category,
                                                          fake_category_repository_cls):
    mocker.patch.object(repositories, 'CategoryRepository', fake_category_repository_cls)

    heureka_test.messages.process_message(FakeTestDayApi(), fake_session, kafka_category_message_without_parent_category)

    category = fake_category_repository_cls(None).get_by_name(
        kafka_category_message_without_parent_category['payload']['name'])

    assert category.name == kafka_category_message_without_parent_category['payload']['name']
    assert category.parent_category is None
