import typing
from collections import defaultdict


class FakeTestDayApi:

    def get_offer_matches(self, offer_id: str) -> typing.List:
        data = defaultdict(list)
        data['feaa400a-d304-4f55-b045-51b1daec8e0c'] = [
            'feaa400a-d304-4f55-b045-51b1daec8e0c',
            '29e0b669-a670-476b-808a-e21a449d1c0f',
            '94fe7882-dd86-424f-b487-706b174d8d4e',
        ]

        data['29e0b669-a670-476b-808a-e21a449d1c0f'] = [
            'feaa400a-d304-4f55-b045-51b1daec8e0c',
            '29e0b669-a670-476b-808a-e21a449d1c0f',
            '94fe7882-dd86-424f-b487-706b174d8d4e',
        ]

        data['94fe7882-dd86-424f-b487-706b174d8d4e'] = [
            'feaa400a-d304-4f55-b045-51b1daec8e0c',
            '29e0b669-a670-476b-808a-e21a449d1c0f',
            '94fe7882-dd86-424f-b487-706b174d8d4e',
        ]

        return data[offer_id]
