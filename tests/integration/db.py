import os
import pytest

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

engine = create_engine(os.environ['DB_CONNECTION_URL'])
Session = sessionmaker(engine)


@pytest.fixture(scope='module')
def connection():
    connection = engine.connect()
    yield connection
    connection.close()


@pytest.fixture(scope='function')
def session(connection):
    os.system('alembic upgrade head')
    yield Session
    os.system('alembic downgrade base')
