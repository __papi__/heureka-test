import pytest
import uuid

from heureka_test import offers
from heureka_test.db import models, repositories


@pytest.fixture
def category(faker):
    return models.Category(
        name=faker.name()
    )


@pytest.fixture
def make_offer(faker, category):
    def _generate():
        return models.Offer(
            uuid=str(uuid.uuid4()),
            category=category,
            name=faker.name(),
            description=faker.paragraph(),
            parameters={},
        )

    return _generate


@pytest.fixture
def offer(faker, make_offer):
    return make_offer()


@pytest.fixture
def make_offer_match(make_offer):
    def _generate():
        source_offer = make_offer()
        target_offer = make_offer()
        return models.OfferMatch(
            source_offer=source_offer,
            target_offer=target_offer,
            common_params_count=offers.find_common_parameters_len(source_offer.parameters, target_offer.parameters),
            diff_params_count=offers.find_diff_parameters_len(source_offer.parameters, target_offer.parameters),
        )

    return _generate


@pytest.fixture
def offer_match(faker, make_offer_match):
    return make_offer_match()


def test_category_repository_add(session, category):
    with session.begin() as session:
        repositories.CategoryRepository(session).add(category)

        categories = session.query(models.Category).all()

    assert len(categories) == 1
    assert categories[0] == category


def test_category_repository_add_or_update_should_add(session, category):
    with session.begin() as session:
        repositories.CategoryRepository(session).add_or_update(category)

        categories = session.query(models.Category).all()

    assert len(categories) == 1
    assert categories[0] == category


def test_category_repository_add_or_update_should_update(faker, session, category):
    with session.begin() as session:
        repositories.CategoryRepository(session).add_or_update(category)  # Should add
        parent_category = models.Category(name=faker.name())
        updated_category = models.Category(
            name=category.name,
            parent_category=parent_category
        )
        repositories.CategoryRepository(session).add_or_update(updated_category)  # Should update

        categories = session.query(models.Category).order_by(models.Category.id).all()

        assert len(categories) == 2
        assert categories[0].name == category.name
        assert categories[0].parent_category.name == parent_category.name
        assert categories[1].name == parent_category.name
        assert categories[1].parent_category is None


def test_category_repository_get(session, category):
    with session.begin() as session:
        session.add(category)
        session.flush([category])
        session.refresh(category)
        returned_category = repositories.CategoryRepository(session).get(category.id)

    assert category == returned_category


def test_category_repository_get_doesnt_exist(faker, session):
    with session.begin() as session:
        assert repositories.CategoryRepository(session).get(faker.random_int()) is None


def test_category_repository_get_by_name_or_create_get(session, category):
    with session.begin() as session:
        session.add(category)
        session.flush([category])
        session.refresh(category)
        assert session.query(models.Category).count() == 1
        returned_category = repositories.CategoryRepository(session).get_by_name_or_create(category.name)
        assert session.query(models.Category).count() == 1
    assert category == returned_category


def test_category_repository_get_by_name_or_create_create(faker, session, category):
    with session.begin() as session:
        assert session.query(models.Category).count() == 0
        returned_category = repositories.CategoryRepository(session).get_by_name_or_create(category.name)
        assert session.query(models.Category).count() == 1
        assert category.name == returned_category.name


def test_category_repository_list(faker, session):
    with session.begin() as session:
        categories = [models.Category(name=faker.name()) for _ in range(faker.random_int(3, 8))]
        for category in categories:
            session.add(category)
            session.flush([category])
            session.refresh(category)

        returned_categories = repositories.CategoryRepository(session).list()
    assert len(returned_categories) == len(categories)
    for category in categories:
        assert category in returned_categories


def test_category_repository_delete(session, category):
    with session.begin() as session:
        session.add(category)
        session.flush([category])
        session.refresh(category)
        assert session.query(models.Category).count() == 1
        repositories.CategoryRepository(session).delete(category)
        assert session.query(models.Category).count() == 0


def test_offer_repository_add(session, offer):
    with session.begin() as session:
        repositories.OfferRepository(session).add(offer)

        offers = session.query(models.Offer).all()

    assert len(offers) == 1
    assert offers[0] == offer


def test_offer_repository_add_or_update_should_add(session, offer):
    with session.begin() as session:
        assert session.query(models.Offer).count() == 0
        repositories.OfferRepository(session).add_or_update(offer)
        assert session.query(models.Offer).count() == 1

        offers = session.query(models.Offer).all()

    assert len(offers) == 1
    assert offers[0] == offer


def test_offer_repository_add_or_update_should_update(faker, session, make_offer, category):
    with session.begin() as session:
        offer = make_offer()
        repositories.OfferRepository(session).add_or_update(offer)  # Should add
        session.flush([offer, offer.category])
        session.refresh(offer)
        updated_offer = models.Offer(
            uuid=offer.uuid,
            category=offer.category,
            name=faker.name(),
            description=faker.paragraph(),
            parameters=[],
        )
        updated_offer = repositories.OfferRepository(session).add_or_update(updated_offer)  # Should update
        session.flush([updated_offer])
        session.refresh(updated_offer)

        offers = session.query(models.Offer).order_by(models.Offer.id).all()

    assert len(offers) == 1
    assert offers[0] == updated_offer


def test_offer_repository_get(session, offer):
    with session.begin() as session:
        session.add(offer)
        session.flush([offer, offer.category])
        session.refresh(offer.category)
        session.refresh(offer)
        returned_offer = repositories.OfferRepository(session).get(offer.id)

    assert offer == returned_offer


def test_offer_repository_get_by_uuid(session, offer):
    with session.begin() as session:
        session.add(offer)
        session.flush([offer, offer.category])
        session.refresh(offer.category)
        session.refresh(offer)
        returned_offer = repositories.OfferRepository(session).get_by_uuid(offer.uuid)

    assert offer == returned_offer


def test_offer_repository_get_by_uuid_doesnt_exist(session, offer):
    with session.begin() as session:
        assert repositories.OfferRepository(session).get_by_uuid(str(uuid.uuid4())) is None


def test_offer_repository_get_doesnt_exist(faker, session):
    with session.begin() as session:
        assert repositories.OfferRepository(session).get(faker.random_int()) is None


def test_offer_repository_list(faker, session, make_offer):
    with session.begin() as session:
        offers = [make_offer() for _ in range(faker.random_int(3, 8))]
        for offer in offers:
            session.add(offer)
            session.flush([offer, offer.category])
            session.refresh(offer.category)
            session.refresh(offer)

        returned_offers = repositories.OfferRepository(session).list()
        assert len(returned_offers) == len(offers)
        for offer in offers:
            assert offer in returned_offers


def test_offer_repository_delete(session, offer):
    with session.begin() as session:
        session.add(offer)
        session.flush([offer, offer.category])
        session.refresh(offer.category)
        session.refresh(offer)
        assert session.query(models.Offer).count() == 1
        repositories.OfferRepository(session).delete(offer)
        assert session.query(models.Offer).count() == 0


def test_offer_match_repository_add(session, offer_match):
    with session.begin() as session:
        repositories.OfferMatchRepository(session).add(offer_match)

        offer_matches = session.query(models.OfferMatch).all()

        assert len(offer_matches) == 1
        assert offer_matches[0] == offer_match


def test_offer_match_repository_add_or_update_should_add(session, offer_match):
    with session.begin() as session:
        assert session.query(models.OfferMatch).count() == 0
        repositories.OfferMatchRepository(session).add_or_update(offer_match)
        assert session.query(models.OfferMatch).count() == 1

        offer_matches = session.query(models.OfferMatch).all()

        assert len(offer_matches) == 1
        assert offer_matches[0] == offer_match


def test_offer_match_repository_add_or_update_should_update(faker, session, offer_match):
    with session.begin() as session:
        repositories.OfferMatchRepository(session).add_or_update(offer_match)  # Should add
        session.flush([offer_match, offer_match.source_offer, offer_match.source_offer.category,
                       offer_match.target_offer, offer_match.target_offer.category])
        session.refresh(offer_match)
        updated_offer_match = models.OfferMatch(
            source_offer_id=offer_match.source_offer_id,
            target_offer_id=offer_match.target_offer_id,
            common_params_count=11,
            diff_params_count=42,
        )
        repositories.OfferMatchRepository(session).add_or_update(updated_offer_match)  # Should update

        offer_matches = session.query(models.OfferMatch).order_by(models.OfferMatch.id).all()

        assert len(offer_matches) == 1
        assert offer_matches[0].common_params_count == 11
        assert offer_matches[0].diff_params_count == 42


def test_offer_match_repository_get(session, offer_match):
    with session.begin() as session:
        session.add(offer_match)
        session.flush([offer_match, offer_match.source_offer, offer_match.source_offer.category,
                       offer_match.target_offer, offer_match.target_offer.category])
        session.refresh(offer_match)
        returned_offer_match = repositories.OfferMatchRepository(session).get(offer_match.id)

        assert offer_match == returned_offer_match


def test_offer_match_repository_get_doesnt_exist(faker, session):
    with session.begin() as session:
        assert repositories.OfferMatchRepository(session).get(faker.random_int()) is None


def test_offer_match_repository_list(faker, session, make_offer_match):
    with session.begin() as session:
        offer_matches = [make_offer_match() for _ in range(faker.random_int(3, 8))]
        for offer_match in offer_matches:
            session.add(offer_match)
            session.flush([offer_match, offer_match.source_offer, offer_match.source_offer.category,
                           offer_match.target_offer, offer_match.target_offer.category])
            session.refresh(offer_match)

        returned_offer_matches = repositories.OfferMatchRepository(session).list()
        assert len(returned_offer_matches) == len(offer_matches)
        for offer_match in offer_matches:
            assert offer_match in returned_offer_matches


def test_offer_match_repository_delete(session, offer_match):
    with session.begin() as session:
        session.add(offer_match)
        session.flush([offer_match, offer_match.source_offer, offer_match.source_offer.category,
                       offer_match.target_offer, offer_match.target_offer.category])
        session.refresh(offer_match)
        assert session.query(models.OfferMatch).count() == 1
        repositories.OfferMatchRepository(session).delete(offer_match)
        assert session.query(models.OfferMatch).count() == 0
