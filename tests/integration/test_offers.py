import pytest
import uuid

from heureka_test import offers
from heureka_test.db import repositories
from tests.fake_testday_api import FakeTestDayApi


@pytest.fixture
def make_offer_kafka_message():
    def _generate():
        pass

    return _generate


def test_create_same_offer_twice(session, faker):
    kwargs = {
        'id': str(uuid.uuid4()),
        'category': faker.name(),
        'name': faker.name(),
        'description': faker.paragraph(),
        'parameters': [],
    }

    offers.create_offer(FakeTestDayApi(), session, **kwargs)
    offers.create_offer(FakeTestDayApi(), session, **kwargs)

    with session.begin() as session:
        offer_list = repositories.OfferRepository(session).list()

        assert len(offer_list) == 1
        assert kwargs['id'] == offer_list[0].uuid
        assert kwargs['category'] == offer_list[0].category.name
        assert kwargs['name'] == offer_list[0].name
        assert kwargs['description'] == offer_list[0].description
        assert kwargs['parameters'] == offer_list[0].parameters


def test_create_same_offer_twice_new_category(session, faker):
    kwargs1 = {
        'id': str(uuid.uuid4()),
        'category': faker.name(),
        'name': faker.name(),
        'description': faker.paragraph(),
        'parameters': [],
    }
    kwargs2 = kwargs1.copy()
    kwargs2['category'] = faker.name()

    offers.create_offer(FakeTestDayApi(), session, **kwargs1)
    offers.create_offer(FakeTestDayApi(), session, **kwargs2)

    with session.begin() as session:
        offer_list = repositories.OfferRepository(session).list()

        assert len(offer_list) == 1
        assert kwargs1['id'] == offer_list[0].uuid
        assert kwargs2['category'] == offer_list[0].category.name
        assert kwargs1['name'] == offer_list[0].name
        assert kwargs1['description'] == offer_list[0].description
        assert kwargs1['parameters'] == offer_list[0].parameters
