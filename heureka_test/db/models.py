from sqlalchemy import Column, ForeignKey, Integer, String, JSON
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Offer(Base):
    __tablename__ = 'offers'
    id = Column(Integer, primary_key=True)
    uuid = Column(String, nullable=False, unique=True)
    category_id = Column(Integer, ForeignKey('categories.id'), nullable=False)
    category = relationship('Category')
    name = Column(String, nullable=False)
    description = Column(String, nullable=False)
    parameters = Column(JSON, nullable=False)

    def to_dict(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'category': self.category.to_dict(),
            'name': self.name,
            'description': self.description,
            'parameters': self.parameters,
        }


class OfferMatch(Base):
    __tablename__ = 'offer_matches'
    id = Column(Integer, primary_key=True)
    source_offer_id = Column(Integer, ForeignKey('offers.id'), nullable=False)
    source_offer = relationship('Offer', foreign_keys=[source_offer_id])
    target_offer_id = Column(Integer, ForeignKey('offers.id'), nullable=False)
    target_offer = relationship('Offer', foreign_keys=[target_offer_id])
    common_params_count = Column(Integer, nullable=False)
    diff_params_count = Column(Integer, nullable=False)

    def to_dict(self):
        return {
            'id': self.id,
            'source_offer': self.source_offer.to_dict(),
            'target_offer': self.target_offer.to_dict(),
            'common_params_count': self.common_params_count,
            'diff_params_count': self.diff_params_count,
        }


class Category(Base):
    __tablename__ = 'categories'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    parent_category_id = Column(Integer, ForeignKey('categories.id'))
    parent_category = relationship('Category', remote_side=[id])

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'parent_category': self.parent_category.to_dict() if self.parent_category else None
        }
