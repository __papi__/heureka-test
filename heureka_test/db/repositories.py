import abc
import typing

from heureka_test.db import models
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import Session


class AbstractRepository(abc.ABC):

    def __init__(self, session: Session):
        self.session = session

    @abc.abstractmethod
    def add(self, obj):
        pass

    @abc.abstractmethod
    def get(self, id: int):
        pass

    @abc.abstractmethod
    def list(self) -> typing.List:
        pass

    @abc.abstractmethod
    def delete(self, obj):
        pass


class OfferRepository(AbstractRepository):
    def add(self, offer: models.Offer):
        self.session.add(offer)

    def add_or_update(self, offer: models.Offer) -> models.Offer:
        try:
            existing_offer = self.session.query(models.Offer).filter(models.Offer.uuid == offer.uuid).one()
            existing_offer.category = offer.category
            existing_offer.name = offer.name
            existing_offer.description = offer.description
            existing_offer.parameters = offer.parameters
            return existing_offer
        except NoResultFound:
            self.add(offer)
            return offer

    def get(self, id: int) -> models.Offer:
        return self.session.query(models.Offer).get(id)

    def get_by_uuid(self, uuid: str) -> models.Offer:
        return self.session.query(models.Offer).filter(models.Offer.uuid == uuid).one_or_none()

    def list(self) -> typing.List[models.Offer]:
        return self.session.query(models.Offer).all()

    def delete(self, offer: models.Offer):
        self.session.delete(offer)


class CategoryRepository(AbstractRepository):
    def add(self, category: models.Category):
        self.session.add(category)

    def add_or_update(self, category: models.Category) -> models.Category:
        try:
            existing_category = self.session.query(models.Category).filter(models.Category.name == category.name).one()
            existing_category.parent_category = category.parent_category
            return existing_category
        except NoResultFound:
            self.add(category)
            return category

    def get(self, id: int):
        return self.session.query(models.Category).get(id)

    def get_by_name_or_create(self, name: str) -> models.Category:
        try:
            category = self.session.query(models.Category).filter(models.Category.name == name).one()
        except NoResultFound:
            category = models.Category(name=name)
            self.add(category)
            self.session.flush([category])
            self.session.refresh(category)

        return category

    def list(self) -> typing.List[models.Category]:
        return self.session.query(models.Category).all()

    def delete(self, category: models.Category):
        self.session.delete(category)


class OfferMatchRepository(AbstractRepository):
    def add(self, offer_match: models.OfferMatch):
        self.session.add(offer_match)

    def add_or_update(self, offer_match: models.OfferMatch):
        try:
            existing_offer_match = self.session.query(models.OfferMatch).filter(
                ((models.OfferMatch.source_offer_id == offer_match.source_offer_id)
                    & (models.OfferMatch.target_offer_id == offer_match.target_offer_id))
                | ((models.OfferMatch.source_offer_id == offer_match.target_offer_id)
                    & (models.OfferMatch.target_offer_id == offer_match.source_offer_id))
            ).one()
            existing_offer_match.common_params_count = offer_match.common_params_count
            existing_offer_match.diff_params_count = offer_match.diff_params_count
            self.session.add(existing_offer_match)
        except NoResultFound:
            self.session.add(offer_match)

    def get(self, id: int) -> models.OfferMatch:
        return self.session.query(models.OfferMatch).get(id)

    def list(self) -> typing.List[models.OfferMatch]:
        return self.session.query(models.OfferMatch).all()

    def delete(self, offer_match: models.OfferMatch):
        self.session.delete(offer_match)
