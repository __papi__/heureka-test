from heureka_test.db import repositories, models


def create_category(Session, name: str, parent_category: str = None, **kwargs):
    """
    Creates new category (and its parent) in the database
    :param Session: Instance of sessionmaker
    :param name: name of the category to be created
    :param parent_category: name of the parent category (will be created if it doesn't exist)
    :param kwargs: in case we get some extra data from Kafka server
    :return: None
    """

    with Session.begin() as session:
        category_repo = repositories.CategoryRepository(session)
        category = models.Category(
            name=name,
            parent_category=category_repo.get_by_name_or_create(parent_category) if parent_category else None
        )
        category_repo.add_or_update(category)
        session.commit()
