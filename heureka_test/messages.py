from heureka_test.categories import create_category
from heureka_test.offers import create_offer
from heureka_test.testday_api import TestDayApi


def process_message(testday_api: TestDayApi, Session, msg: dict):
    """
    Processes message from kafka server
    :param testday_api: Instance of TestDayApi
    :param Session: Instance of sessionmaker
    :param msg: message from kafka server
    :return: None
    """

    if msg['metadata']['type'] == 'offer':
        create_offer(testday_api, Session, **msg['payload'])
    elif msg['metadata']['type'] == 'category':
        create_category(Session, **msg['payload'])
