from sqlalchemy.orm.session import Session

from heureka_test.db import repositories, models
from heureka_test.testday_api import TestDayApi


def find_common_parameters_len(source_params: dict, target_params: dict):
    """
    Compares given dictionaries and returns number of common key-value pairs
    :param source_params: source dictionary that will be compared with target dictionary
    :param target_params: against this dictionary will be source_params compared
    :return: number of common key-value pairs
    """
    common = 0
    for param_name, value in source_params.items():
        if param_name in target_params and target_params[param_name] == value:
            common += 1

    return common


def find_diff_parameters_len(source_params: dict, target_params: dict):
    """
    Compares given dictionaries and returns number of different key-value pairs
    :param source_params: source dictionary that will be compared with target dictionary
    :param target_params: against this dictionary will be source_params compared
    :return: number of different key-value pairs
    """
    diff = 0
    for param_name, value in source_params.items():
        if param_name not in target_params or (param_name in target_params and target_params[param_name] != value):
            diff += 1

    # We also need to add number of missing or extra entries
    return diff + max(0, len(target_params) - len(source_params))


def create_offer(testday_api, Session, id: str, category: str, name: str, description: str, parameters: dict, **kwargs):
    """
    Creates new offer (and its category in case it doesn't exist) in the database
    :param testday_api: Instance of TestDayApi
    :param Session: Instance of sessionmaker
    :param id: remote identifier of the offer
    :param category: category that offer belongs to
    :param name: name of the offer
    :param description: short description of the offer
    :param parameters: dictionary of parameters of the offer
    :param kwargs: in case we get some extra data from Kafka server
    :return:
    """
    with Session.begin() as session:
        offer = models.Offer(uuid=id, category=repositories.CategoryRepository(session).get_by_name_or_create(category),
                             name=name, description=description, parameters=parameters)
        offer = repositories.OfferRepository(session).add_or_update(offer)

        # Force saving new offer to the database
        session.flush([offer])
        session.refresh(offer)
        update_matching_offers(testday_api, session, offer)
        session.commit()


def update_matching_offers(testday_api: TestDayApi, session: Session, source_offer: models.Offer):
    """
    Updates matching offer record for given source_offer
    :param testday_api: Instance of TestDayApi
    :param session: Instance of sessionmaker
    :param source_offer: Offer for which should this method find matching offers
    :return:
    """
    offer_repo = repositories.OfferRepository(session)
    for matching_offer_uuid in testday_api.get_offer_matches(source_offer.uuid):
        target_offer = offer_repo.get_by_uuid(matching_offer_uuid)
        if not target_offer:
            # Target offer doesn't exist
            continue

        if source_offer == target_offer:
            # Same offers have always all parameters in common, we don't need to add this information to the database
            continue

        offer_match = models.OfferMatch(
            source_offer=source_offer,
            target_offer=target_offer,
            common_params_count=find_common_parameters_len(source_offer.parameters, target_offer.parameters),
            diff_params_count=find_diff_parameters_len(source_offer.parameters, target_offer.parameters),
        )
        repositories.OfferMatchRepository(session).add_or_update(offer_match)
