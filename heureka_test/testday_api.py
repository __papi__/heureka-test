import requests
import typing

from urllib.parse import urljoin


class TestDayApi:
    def __init__(self, endpoint: str, auth_token: str):
        self.endpoint = endpoint
        self.auth_token = auth_token

    def _get_headers(self) -> dict:
        return {
            'Auth': self.auth_token
        }

    def get_offer_matches(self, offer_id: str) -> typing.List:
        """
        Returns list of matching offers for given offer id
        :param offer_id: offer id obtained from kafka server
        :return: list of matching offer ids
        """

        response = requests.get(urljoin(self.endpoint, f'offer-matches/{offer_id}'), headers=self._get_headers())
        if response.status_code != 200:
            return []

        return response.json()['matching_offers']
