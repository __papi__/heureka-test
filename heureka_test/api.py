from flask import Flask, abort

from heureka_test.db import repositories


def create_app(Session):
    app = Flask(__name__)

    @app.route('/offers', methods=('GET',))
    def get_offers_list():
        with Session.begin() as session:
            return {'offers': [x.to_dict() for x in repositories.OfferRepository(session).list()]}

    @app.route('/offers/<int:offer_id>', methods=('GET',))
    def get_offer(offer_id):
        with Session.begin() as session:
            offer = repositories.OfferRepository(session).get(offer_id)
            if not offer:
                return abort(404)
            return {'offer': offer.to_dict()}

    @app.route('/categories', methods=('GET',))
    def get_categories_list():
        with Session.begin() as session:
            return {'categories': [x.to_dict() for x in repositories.CategoryRepository(session).list()]}

    @app.route('/categories/<int:category_id>', methods=('GET',))
    def get_category(category_id):
        with Session.begin() as session:
            category = repositories.CategoryRepository(session).get(category_id)
            if not category:
                return abort(404)
            return {'category': category.to_dict()}

    @app.route('/offer-matches', methods=('GET',))
    def get_offer_matches_list():
        with Session.begin() as session:
            return {'offer_matches': [x.to_dict() for x in repositories.OfferMatchRepository(session).list()]}

    @app.route('/offer-matches/<int:offer_match_id>', methods=('GET',))
    def get_offer_match(offer_match_id):
        with Session.begin() as session:
            offer_match = repositories.OfferMatchRepository(session).get(offer_match_id)
            if not offer_match:
                return abort(404)
            return {'offer_match': offer_match.to_dict()}

    return app
