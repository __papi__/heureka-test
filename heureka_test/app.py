import json
import logging
import os
import prometheus_client

from kafka import KafkaConsumer
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from heureka_test import api
from heureka_test.messages import process_message
from heureka_test.testday_api import TestDayApi


logger = logging.getLogger(__name__)
engine = create_engine(os.environ['DB_CONNECTION_URL'])
Session = sessionmaker(engine)


def main():
    testday_api = TestDayApi(os.environ['TESTDAY_API_ENDPOINT'], os.environ['TESTDAY_API_AUTH_TOKEN'])
    prometheus_client.start_http_server(int(os.environ['PROMETHEUS_CLIENT_PORT']))
    consumer = KafkaConsumer(
        os.environ['KAFKA_TOPIC'],
        bootstrap_servers=os.environ['KAFKA_BOOTSTRAP_SERVERS'],
        security_protocol=os.environ['KAFKA_SECURITY_PROTOCOL'],
        sasl_mechanism=os.environ['KAFKA_SASL_MECHANISM'],
        sasl_plain_username=os.environ['KAFKA_USERNAME'],
        sasl_plain_password=os.environ['KAFKA_PASSWORD'],
        auto_offset_reset='earliest',
    )

    for msg in consumer:
        process_message(testday_api, Session, json.loads(msg.value))


api_app = api.create_app(Session)

if __name__ == '__main__':
    logging.basicConfig()
    main()
