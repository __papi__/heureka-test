#!/bin/bash

while ! nc -z database 5432; do sleep 1; done; # Wait for PostgreSQL

alembic upgrade head

python heureka_test/app.py
